package fr.gouv.data.recherche.anr.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
public class AnrBatchApplication {

    public static void main(String[] args) {
        SpringApplication.run(AnrBatchApplication.class, args);
    }

}
