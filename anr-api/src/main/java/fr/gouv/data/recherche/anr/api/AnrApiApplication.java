package fr.gouv.data.recherche.anr.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnrApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(AnrApiApplication.class, args);
    }

}
