# ANR Projects

## Description

The current project aim to help Dataverse contributors to add metadata about project and funding related to their Dataset.

This help is designed to allow to search for an ANR Project informations by its code with an auto-completion system.

Selecting an ANR Project will fill other fields such as title, acronym, funding agency, identifiers.

## Integration

The integration into the Dataverse software is described [here](dataverse/readme.md)