# Integration in Dataverse

Integration in Dataverse depends on 2 files : *cvoc-conf.json* and *anr_projects.js*

*anr_projects.js* contains specific javascript selectors that are specific to a Dataverse version and citation.tsv metadatablock customization.

## Enable Dataverse configuration

To add this module to your Dataverse you must add this configuration :

`curl -X PUT --upload-file cvoc-conf.json http://localhost:8080/api/admin/settings/:CVocConf`

Also, you need to customize *cvoc-conf.json* with your path to *anr_projects.js*, ex: `"js-url": "https://entrepot.recherche.data.gouv.fr/assets/cvoc/anr_projects.js"`

## Select2

*anr_projects.js* script uses https://select2.org/ and it is automatically loaded by Dataverse while using `:CVocConf` setting.

# Development

## Rules

The user interface behavior rules of *anr_projects.js* script are specified in file [rules.md](rules.md)